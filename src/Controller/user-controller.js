const {
    getAllUsers,
    getUser,
    addUser,
    updateUser,
    deleteUser,
    checkEmail: checkEmailFromDB,
    getUserByEmail,
    getEmail
} = require('../Repository/user-repository');

const {
    hashPassword,
    verifyPassword
} = require('../lib/password');


const {
    userValidation
} = require('../Model/user');

const {
    requireLogin
} = require('../Middleware/user-middleware');


class UserController {
    getAllUser = async (req, res) => {
        const users = await getAllUsers();
        res.send({
            data: users,
            message: `Get all infomation user`
        });
    }

    getUser = async (req, res) => {
        // const sess = req.session;
        // if (!sess.user) {
        //     res.status(401).send({
        //         message: "Please login first"
        //     })
        // }
        const {
            id
        } = req.params;
        const user = await getUser(id);
        if (!user) {
            res.status(404).send({
                data: {},
                message: `Not found`
            });
            return;
        }
        res.send({
            data: user,
            message: `Info user id: ${id}`
        });
    }

    addUser = async (req, res) => {
        // Get value from request
        const {
            email,
            password,
            firstName,
            lastName,
            phoneNumber
        } = req.body;
        // Fresh value
        const emailClean = email.trim().toLowerCase();
        const firstNameClean = firstName.trim();
        const lastNameClean = lastName.trim();
        // Check Value
        const checkEmail = userValidation.validateEmail(emailClean);
        const checkPassword = userValidation.validatePassword(password);
        const checkName = userValidation.validateName(firstNameClean, lastNameClean);
        const checkPhoneNumber = userValidation.validatePhoneNumber(phoneNumber);

        // Check value return is FALSE?
        if (!checkEmail.valid || !checkPassword.valid || !checkName.valid || !checkPhoneNumber.valid) {
            res.status(400).send({
                message: checkEmail.message || checkPassword.message || checkName.message || checkPhoneNumber.message,
            });
            return;
        }

        // Call function check Email is exist?
        if (await checkEmailFromDB(emailClean)) {
            res.status(400).send({
                message: "Email existed"
            });
            return;
        }

        // Declare variable to hash password ex: 1234 => DJshsaoas234....
        const hash = await hashPassword(password);

        // Declare variable to add user with all info on top
        const newUser = await addUser(emailClean, hash, firstName, lastName, phoneNumber);

        // Send data and message for user see
        res.send({
            data: newUser,
            message: "OK"
        });
    }

    updateUser = async (req, res) => {
        // Get all info need update
        const {
            email,
            password,
            firstName,
            lastName,
            phoneNumber
        } = req.body;
        // Get info user need change on top
        const {
            id
        } = req.params;

        // Fresh value
        const emailClean = email.trim().toLowerCase();
        const firstNameClean = firstName.trim();
        const lastNameClean = lastName.trim();
        // Check all info before save DB
        const checkEmail = userValidation.validateEmail(emailClean);
        const checkPassword = userValidation.validatePassword(password);
        const checkName = userValidation.validateName(firstNameClean, lastNameClean);
        const checkPhoneNumber = userValidation.validatePhoneNumber(phoneNumber);

        // 
        if (!checkEmail.valid || !checkPassword.valid || !checkName.valid || !checkPhoneNumber.valid) {
            res.status(400).send({
                message: checkEmail.message || checkPassword.message || checkName.message || checkPhoneNumber.message,
            });
            return;
        }
        // 
        if (!await getUser(id)) {
            res.status(404).send({
                message: `Not found user id ${id}`
            });
            return;
        }

        const listUser = await getEmail();
        listUser.forEach(email => {
            if (email == emailClean) {
                res.status(400).send({
                    message: "Email has existed"
                });
                return;
            }
        });

        const hash = await hashPassword(password);

        // Start update
        const newUser = updateUser(id, emailClean, hash, firstNameClean, lastNameClean, phoneNumber);
        // Return info
        res.send({
            data: newUser,
            message: `update user success`
        });

    }

    deleteUser = async (req, res) => {
        const {
            id
        } = req.params;
        if (!await getUser(id)) {
            res.status(404).send({
                message: `User id ${id} not existed`
            });
            return;
        }

        const userDelete = await deleteUser(id);
        res.send({
            data: userDelete,
            message: `Delete success user id: ${id}`
        })
    }


    login = async (req, res) => {
        // Get email, password from input
        const {
            email,
            password
        } = req.body;
        const emailClean = email.trim().toLowerCase();
        // Check Email (trim, exist, regular expression)
        const checkEmail = userValidation.validateEmail(emailClean);
        const checkPassword = userValidation.validatePassword(password);
        // Get Password from DB (Select password from users where email = emailInput)
        if (!checkEmail.valid || !checkPassword.valid) {
            res.status(400).send({
                message: "Email or Password not correct"
            });
            return;
        }

        const user = await getUserByEmail(emailClean);

        

        if (!user) {
            res.status(400).send({
                message: "User not found"
            });
            return;
        }
        // CheckPassword (verify password(passwordInput, passwordDB))
        if (!(await verifyPassword(password, user.password))) {
            res.status(400).send({
                message: "Password not correct"
            });
            return;
        }
        // OK

        if (!req.session.user) {
            req.session.user = user
        }

        console.log(req.sessionID);

        res.send({
            data: req.session.user,
            sessionID: req.sessionID,
            message: "Login successfully"
        });
        
    }

    logout = async (req, res) => {
        console.log(req.session.user)
        if (!req.session) {
           res.status(401).send({
               message: "Logout failure"
           });
           return;
        }

        req.session.destroy();
        res.send({
            message: "Logout success"
        });
    }
}

// Dùng session lưu info user login
// Cập nhật user (PUT) => Check đã login hay chưa, cập nhật đúng hay không theo id
// Thêm column (firstName, lastName, Phone) => OK

module.exports = new UserController;