const pool = require('../DB/connectDB');

const getAllUsers = async () => {
    const client = await pool.connect();
    try {
        const res = await client.query('SELECT id, email, firstName, lastName, phoneNumber FROM users');
        console.log(res.rows);
        return res.rows;
    } finally {
        // Make sure to release the client before any error handling,
        // just in case the error handling itself throws an error.
        client.release();
    }
}

const getUser = async (id) => {
    const client = await pool.connect();
    try {
        const res = await client.query('SELECT id, email, firstName, lastName, phoneNumber FROM users Where id = $1', [id]);
        console.log(res.rows[0]);
        return res.rows[0];
    } finally {
        // Make sure to release the client before any error handling,
        // just in case the error handling itself throws an error.
        client.release();
    }
}

const addUser = async (email, password, firstName, lastName, phoneNumber) => {
    const client = await pool.connect()
    try {
        const res = await client.query('Insert into users (email, password, firstname, lastname, phonenumber) Values ($1, $2, $3, $4, $5) RETURNING id', [email, password, firstName, lastName, phoneNumber])
        console.log(res.rows)
        return res.rows[0];
    } finally {
        // Make sure to release the client before any error handling,
        // just in case the error handling itself throws an error.
        client.release()
    }
}

const updateUser = async (id, email, password, firstName, lastName, phoneNumber) => {
    const client = await pool.connect()
    try {
        const res = await client.query('Update users set email = $1, password = $2, firstname = $3, lastname = $4, phonenumber = $5 Where id = $6', [email, password, firstName, lastName, phoneNumber, id]);
        console.log(res.rowCount)
        return res.rowCount;
    } finally {
        // Make sure to release the client before any error handling,
        // just in case the error handling itself throws an error.
        client.release()
    }
}

const deleteUser = async (id) => {
    const client = await pool.connect()
    try {
        const res = await client.query('DELETE FROM users WHERE id = $1 RETURNING *', [id]);
        console.log(res.rowCount)
        return res.rowCount;
    } finally {
        // Make sure to release the client before any error handling,
        // just in case the error handling itself throws an error.
        client.release()
    }
}


const checkEmail = async (email) => {
    const client = await pool.connect();
    try {
        const res = await client.query('Select id from users where email = $1', [email]);
        return res.rows.length > 0;

    } finally {
        client.release();
    }
}

const checkPhoneNumber = async (phoneNumber) => {
    const client = await pool.connect();
    try {
        const res = await client.query('Select id from users where email = $1', [phoneNumber]);
        return res.rows.length > 0;

    } finally {
        client.release();
    }
}

const getUserByEmail = async (email) => {
    const client = await pool.connect();
    try {
        const res = await client.query('Select * from users where email = $1', [email]);
        return res.rows[0];

    } finally {
        client.release();
    }
}

const getEmail = async () => {
    const client = await pool.connect();
    try {
        const res = await client.query('Select email from users');
        return res.rows;

    } finally {
        client.release();
    }
}

module.exports = {
    getAllUsers,
    getUser,
    addUser,
    updateUser,
    deleteUser,
    checkEmail,
    getUserByEmail,
    checkPhoneNumber,
    getEmail
};