const express = require('express');
const session = require('express-session');

const route = express.Router();
const userController = require('../Controller/user-controller');
const {
    requireLogin,
    requireLoginById
} = require('../Middleware/user-middleware')

route.get("/", (req, res) => {
    res.render("index");
});

route.use(session({
    secret: "SecretKey",
    resave: true,
    saveUninitialized: true,
    cookie: {
        secure: false,
        maxAge: 60000
    }
}));

route.get("/user", userController.getAllUser);

route.get("/user/:id", requireLoginById, userController.getUser);

route.post("/user", userController.addUser);

route.put("/user/:id", requireLoginById, userController.updateUser);

route.delete("/user/:id", requireLoginById, userController.deleteUser);

route.post("/login", userController.login);

route.post("/logout", requireLogin, userController.logout);

module.exports = route;
// Middware