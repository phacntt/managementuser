const requireLogin = (req, res, next) => {
    if (!req.session || !req.session.user) {
        res.status(401).send({
            message: "You must login. Please login again",
        });
        return;
    }

    next();
}

const requireLoginById = (req, res, next) => {
    const {
        id
    } = req.params;
    
    if (!req.session || !req.session.user) {
        res.status(401).send({
            message: "You must login. Please login again",
        });
        return;
    }

    if (req.session.user.id != id) {
        res.status(401).send({
            message: "You not permission"
        });
        return;
    }
    next();
}


module.exports = {
    requireLogin,
    requireLoginById
}