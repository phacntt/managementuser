CREATE TABLE public."Users"
(
    email text,
    id bigserial,
    password text,
    PRIMARY KEY (id)
);
