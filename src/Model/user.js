const REGMAIL = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const MIN_PASSWORD_LENGTH = 6;
const REPHONENUMBER = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/;
const MIN_PHONENUMBER = 10;
validateEmail = (email) => {
    if (!email) {
        return {
            valid: false,
            message: "Email phải điền không được để trống"
        };
    }
    
    if (!REGMAIL.test(String(email).toLowerCase())) {
        return {
            valid: false,
            message: "Not correct form email"
        }
    } 

    return {
        valid: true,
        message: ""
    };
};

validatePassword = (password) => {
    if (!password || password.length < MIN_PASSWORD_LENGTH) {
        return {
            valid: false,
            message: "Password phải được điền không được để trống"
        };
    }


    return {
        valid: true,
        message: ""
    };
}

validatePhoneNumber = (phoneNumber) => {
    if (!phoneNumber || phoneNumber < MIN_PHONENUMBER) {
        return {
            valid: false,
            message: "PhoneNumber must fill in field and lenght not min 10"
        };
    }

    if (!REPHONENUMBER.test(String(phoneNumber))) {
        return {
            valid: false,
            message: "PhoneNumber not correct form"
        }
    }

    return {
        valid: true,
        message: ""
    }
}

validateName = (firstName, lastName) => {
    if (!firstName || !lastName) {
        return {
            valid: false,
            message: "first name or last name must fill in form"
        }
    }
    return {
        valid: true,
        message: ""
    }
}

module.exports = {
    userValidation: {
        validateEmail,
        validatePassword,
        validatePhoneNumber,
        validateName
    }
}