const express = require('express');
const router = require('./src/Route/user-route');
const app = express();

app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public'));
app.use(express.json());
app.use(express.urlencoded({
    extended: true
}));


app.use('/', router);

app.listen(3000, () => {
    console.log("The Server is listening on Port 3000");
});