# ManagementUser



## Getting started

The first my project basic code API use NodeJS, ExpressJS, PostgreSQL, JWT, Brcypt

## Prepared

Install PostgreSQL in [here](https://www.postgresql.org/download/) <br>
Configs PostgreSQL in [here](https://openplanning.net/10713/cai-dat-co-so-du-lieu-postgresql-tren-windows) <br>

# Setting database
Open pgAdmin4 -> Enter password -> Click Servers -> Databases -> Click right mouse -> Create -> Database... -> Enter name Database is 'ManagementUser' -> Save -> Click Database just create -> Click right button -> Query Tool -> Wayback folder clone open file init.sql in src/DB/init.sql and copy all to paste in query tool -> Press F5 to run 

## Test and Deploy
npm install

npm start

go to  http://localhost:3000

## Usage
NodeJS, ExpressJS, PostgreSQL, JWT (JSON Web Token), Bcrypt

## Support
Gmail: trananhpha2001@gmail.com

## License
License: ISC
